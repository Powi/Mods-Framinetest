-- Rubber mod for Minetest
-- Copyright 2012 Mark Holmquist <mtraceur@member.fsf.org>
--
-- The Rubber mod is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- The Rubber mod is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with the Rubber mod. If not, see <http://www.gnu.org/licenses/>.

-- Boilerplate to support localized strings if intllib mod is installed.
local S
if (minetest.get_modpath("intllib")) then
  dofile(minetest.get_modpath("intllib").."/intllib.lua")
  S = intllib.Getter(minetest.get_current_modname())
else
  S = function ( s ) return s end
end

minetest.register_node("rubber:rubber_sapling", {
	description = S("Rubber tree sapling"),
	drawtype = "plantlike",
	tiles = {"rubber_sapling.png"},
	inventory_image = "rubber_sapling.png",
	paramtype = "light",
	walkable = false,
	groups = {dig_immediate=3,flammable=2},
	sounds = default.node_sound_defaults(),
})

minetest.register_node("rubber:rubber_tree_full", {
	description = S("Rubber tree"),
	tiles = {"default_tree_top.png", "default_tree_top.png", "rubber_tree_full.png"},
	groups = {tree=1,snappy=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	drop = "default:tree",
	sounds = default.node_sound_wood_defaults(),

	on_dig = function(pos, node, digger)
		minetest.node_dig(pos, node, digger)
		minetest.env:remove_node(pos)
	end,

	after_destruct = function(pos, oldnode)
		oldnode.name = "rubber:rubber_tree_empty"
		minetest.env:set_node(pos, oldnode)
	end
})


minetest.register_node("rubber:rubber_tree_empty", {
	tiles = {"default_tree_top.png", "default_tree_top.png", "rubber_tree_empty.png"},
	groups = {tree=1,snappy=1,choppy=2,oddly_breakable_by_hand=1,flammable=2, not_in_creative_inventory=1},
	drop = "default:tree",
	sounds = default.node_sound_wood_defaults(),
})

minetest.register_abm({
	nodenames = {"rubber:rubber_tree_empty"},
	interval = 60,
	chance = 15,
	action = function(pos, node)
		node.name = "rubber:rubber_tree_full"
		minetest.env:set_node(pos, node)
	end
})

minetest.register_node("rubber:rubber_leaves", {
	drawtype = "allfaces_optional",
	visual_scale = 1.3,
	tiles = {"default_leaves.png"},
	paramtype = "light",
	groups = {snappy=3, leafdecay=3, flammable=2, not_in_creative_inventory=1},
	drop = {
		max_items = 1,
		items = {
			{
				items = {'rubber:rubber_sapling'},
				rarity = 20,
			},
		}
	},
	sounds = default.node_sound_leaves_defaults(),
})

minetest.register_abm({
	nodenames = {"rubber:rubber_sapling"},
	interval = 60,
	chance = 20,
	action = function(pos, node)
		farming:generate_tree(pos, "rubber:rubber_tree_full", "rubber:rubber_leaves", {"default:dirt", "default:dirt_with_grass"})
	end
})

minetest.register_on_generated(function(minp, maxp, blockseed)
	if math.random(1, 100) > 5 then
		return
	end
	local tmp = {x=(maxp.x-minp.x)/2+minp.x, y=(maxp.y-minp.y)/2+minp.y, z=(maxp.z-minp.z)/2+minp.z}
	local pos = minetest.env:find_node_near(tmp, maxp.x-minp.x, {"default:dirt_with_grass"})
	if pos ~= nil then
		farming:generate_tree({x=pos.x, y=pos.y+1, z=pos.z}, "rubber:rubber_tree_full", "rubber:rubber_leaves", {"default:dirt", "default:dirt_with_grass"})
	end
end)

minetest.register_craftitem("rubber:bucket_rubber", {
	description = S("Bucket of rubber"),
	inventory_image = "bucket_rubber.png",
	stack_max = 1,
})

local bucket_tmp = {
	source = "rubber:rubber_tree_full",
	itemname = "rubber:bucket_rubber"
}
bucket.liquids["rubber:rubber_tree_full"] = bucket_tmp

-- ========= FUEL =========
minetest.register_craft({
	type = "fuel",
	recipe = "rubber:rubber_sapling",
	burntime = 10
})

