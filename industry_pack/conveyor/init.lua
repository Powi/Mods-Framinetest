-- Conveyor mod for Minetest
-- Copyright 2012 Mark Holmquist <mtraceur@member.fsf.org>
--
-- The Conveyor mod is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- The Conveyor mod is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with the Conveyor mod. If not, see <http://www.gnu.org/licenses/>.

-- Boilerplate to support localized strings if intllib mod is installed.
local S
if (minetest.get_modpath("intllib")) then
  dofile(minetest.get_modpath("intllib").."/intllib.lua")
  S = intllib.Getter(minetest.get_current_modname())
else
  S = function ( s ) return s end
end

minetest.register_craftitem( 'conveyor:conveyor', {
    description = S('Conveyor belt'),
    inventory_image = 'conveyor.png',
} )

minetest.register_craft( {
    output = 'conveyor:conveyor',
    recipe = {
        { 'rubber_sheet:rubber_sheet', 'rubber_sheet:rubber_sheet', 'rubber_sheet:rubber_sheet' },
        { 'gears:gear', 'gears:gear', 'gears:gear' },
        { 'rubber_sheet:rubber_sheet', 'rubber_sheet:rubber_sheet', 'rubber_sheet:rubber_sheet' }
    }
} )

