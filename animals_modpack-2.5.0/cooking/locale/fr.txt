# French translation for Animals Modpack.
# Traduction en Français d’Animals Modpack.
# Author/Auteur: Nicolas « Powi » Denis

Cooked Meat = Viande Cuite
Cooked Pork Meat = Viande de Porc cuite
Cooked Chicken = Poulet Cuit
Cooked Beef = Steak de bœuf
Cooked Meat (Now Dead) = Viande Cuite (totalement morte, maintenant)
Cooked Venison Meat = Venaison cuite 
Cooked Toxic Meat = Viande Toxique cuite
Cooked Bluewhite Meat = Poisson bleu-blanc cuit
Cooked Meat = Viande Cuite
Cooked Porkchop = Cote de Porc cuite
