#Traduis par GuePE, Mijak et un tout petit peu par Powi.

Admin Shield = Bouclier de l’Admin
Wooden Shield = Bouclier en Bois
Enhanced Wood Shield = Bouclier amélioré en Bois
Cactus Shield = Bouclier en Cactus
Enhanced Cactus Shield = Bouclier amélioré en Cactus
Steel Shield = Bouclier en Acier
Bronze Shield = Bouclier en Bronze
Diamond Shield = Bouclier en Diamant
Gold Shield = Bouclier en Or
Mithril Shield = Bouclier en Mithril
Crystal Shield = Bouclier en Cristal
